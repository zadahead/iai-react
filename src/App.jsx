import { NavLink, Route, Routes } from 'react-router-dom';
import './App.css';

import HomeView from './Views/HomeView';
import AboutView from './Views/AboutView';
import StateView from './Views/StateView';
import UsersView from './Views/UsersView';

import { Icon, Between, Line, Grid, Rows } from 'UIKit';
import { CycleView } from 'Views/CycleView';
import { FetchView } from 'Views/FetchView';
import { RefView } from 'Views/RefView';
import { DropdownView } from 'Views/DropdownView';
import { HooksView } from 'Views/HooksView';
import { useContext } from 'react';
import { countContext } from 'Context/countContext';
import { colorSwitchContext } from 'Context/colorSwitchContext';
import { useSelector } from 'react-redux';
import { TodosView } from 'Views/TodosView';

const App = () => {
    const { count } = useContext(countContext);
    const { color } = useContext(colorSwitchContext);
    const store = useSelector((state) => state);

    const getCompleted = () => {
        return store.todos.list.filter(i => i.completed).length;
    }

    return (
        <div className='App'>
            <Grid>
                <Between>
                    <Line>
                        <Icon>settings</Icon>
                        <h4 style={{ color }}>{count}</h4>
                        <h4 style={{ color: store.color.color }}>#{store.count.value}</h4>
                        <h4>{getCompleted()}</h4>
                    </Line>
                    <Line>
                        {/* <NavLink to='/states'>States</NavLink> */}
                        <NavLink to='/users'>Users</NavLink>
                        <NavLink to='/cycle'>Cycle</NavLink>
                        <NavLink to='/fetch'>Fetch</NavLink>
                        <NavLink to='/ref'>Ref</NavLink>
                        <NavLink to='/dropdown'>Dropdown</NavLink>
                        <NavLink to='/hooks'>Hooks</NavLink>
                        <NavLink to='/todos'>Todos</NavLink>
                    </Line>
                </Between>

                <Routes>
                    <Route path='/hello' element={<HomeView />} />
                    <Route path='/about' element={<AboutView />} />
                    <Route path='/states' element={<StateView />} />
                    <Route path='/users' element={<UsersView />} />
                    <Route path='/cycle' element={<CycleView />} />
                    <Route path='/fetch' element={<FetchView />} />
                    <Route path='/dropdown' element={<DropdownView />} />
                    <Route path='/ref' element={(
                        <Rows>
                            <h1>Will this be red?</h1>
                            <RefView />
                            <RefView />
                        </Rows>
                    )} />
                    <Route path='/hooks' element={<HooksView />} />
                    <Route path='/todos' element={<TodosView />} />
                    <Route path='/*' element={<h2>no page</h2>} />
                </Routes>
            </Grid>
        </div>
    )
}

export default App;