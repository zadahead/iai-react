const Box = (props) => {

    const styleCss = {
        backgroundColor: 'red',
        color: 'yellow',
        border: '1px solid #e1e1e1'
    }

    return (
        <div>
            <h1>{props.title}</h1>
            <h2>{props.info}</h2>
            <div style={styleCss}>
                {props.children}
            </div>
        </div>
    )
}

export default Box;