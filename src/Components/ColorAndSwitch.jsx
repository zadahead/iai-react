import { useState } from "react"
import Btn from "../UIKit/Elements/Btn/Btn"
import Line from "../UIKit/Layouts/Line/Line"

const ColorAndSwitch = () => {
    const [count, setCount] = useState(0);
    const [isRed, setIsRed] = useState(true);

    const handleAdd = () => {
        setCount(count + 1);
    }

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }

    return (
        <div>
            <h3 style={styleCss}>Count, {count}</h3>
            <Line>
                <Btn onClick={handleAdd}>Add</Btn>
                <Btn onClick={handleSwitch}>Switch</Btn>
            </Line>
        </div>
    )
}

export default ColorAndSwitch;