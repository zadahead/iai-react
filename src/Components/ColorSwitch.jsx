import { useState } from "react";
import { Btn } from "UIKit";

const ColorSwitch = () => {
    const [isRed, setIsRed] = useState(true);

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    const styleCss = {
        width: '100px',
        height: '100px',
        backgroundColor: isRed ? 'red' : 'blue'
    }

    return (
        <div>
            <h3>Color Switch</h3>
            <div style={styleCss}></div>
            <Btn onClick={handleSwitch}>Switch</Btn>
        </div>
    )
}

export default ColorSwitch;