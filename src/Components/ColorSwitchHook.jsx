import { useColorSwitch } from "Hooks/useColorSwitch";
import { Btn, Rows } from "UIKit";


export const ColorSwitchHook = () => {
    //logic
    const { color, handleSwitch } = useColorSwitch();


    //view
    const styleCss = {
        backgroundColor: color,
        width: '100px',
        height: '100px'
    }

    return (
        <Rows>
            <div style={styleCss}></div>
            <div>
                <Btn onClick={handleSwitch}>Switch</Btn>
            </div>
        </Rows>
    )
}