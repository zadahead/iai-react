

import { Btn, Line } from "UIKit";

import ColorSwitch from "./ColorSwitch";

const Counter = (props) => {


    const handleAdd = () => {
        props.setCount(props.count + props.add);
    }

    const handleSub = () => {
        props.setCount(props.count - props.add);
    }

    const renderBoom = () => {
        if (props.count > 14 && props.count < 20) {
            return <ColorSwitch />;
        }
    }

    return (
        <div>
            <Line>
                <h3>Count, {props.count}</h3>
                <Btn onClick={handleAdd}>Add</Btn>
                <Btn onClick={handleSub}>Sub</Btn>
                {renderBoom()}
            </Line>

        </div>
    )
}

export default Counter;