import { useColorSwitch } from "Hooks/useColorSwitch";
import { useCounter } from "Hooks/useCounter"
import { Btn, Rows } from "UIKit"

export const CounterColor = () => {
    const { count, handleAdd } = useCounter();
    const { color, handleSwitch } = useColorSwitch();

    const handleClick = () => {
        handleAdd();
        handleSwitch();
    }

    const styleCss = {
        color
    }
    return (
        <Rows>
            <h3 style={styleCss}>Count, {count}</h3>
            <div>
                <Btn onClick={handleClick}>Add + Switch</Btn>
            </div>
        </Rows>
    )
}