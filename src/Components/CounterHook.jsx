import { useCounter } from "Hooks/useCounter";
import { Btn, Rows } from "UIKit";

/*
    1) create a fully working component
    2) mark the logic section and the view
    3) create a "useXXX" function
    4) paste all the logic to the useXXX hook
    5) return from the useXXX hook, all the values the view needs
    6) call the useXXX hook and use the values
    7) export the useXXX from /Hooks folder
*/

/*
 export const CounterHook = () => {
    //logic
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    //view
    return (
        <Rows>
            <h3>Count, {count}</h3>
            <div>
                <Btn onClick={handleAdd}>Add</Btn>
            </div>
        </Rows>
    )
}
*/



export const CounterHook = () => {
    //logic
    const { count, handleAdd } = useCounter();

    //view
    return (
        <Rows>
            <h3>Count, {count}</h3>
            <div>
                <Btn onClick={handleAdd}>Add</Btn>
            </div>
        </Rows>
    )
}