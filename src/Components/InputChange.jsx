import { useState } from "react";
import Btn from "../UIKit/Elements/Btn/Btn"
import Line from "../UIKit/Layouts/Line/Line"

const InputChange = () => {
    console.log('render');

    const [count, setCount] = useState(10);

    const handleAdd = () => {
        setCount(count + 1);
    }

    const handleChange = (e) => {
        setCount(+e.target.value);
    }

    return (
        <div>
            <h3>Current Count, {count}</h3>
            <Line>
                <input value={count} onChange={handleChange} />
                <Btn onClick={handleAdd}>Add</Btn>
            </Line>
        </div>
    )
}

export default InputChange;