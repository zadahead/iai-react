import { useState } from "react";
import { Btn } from "UIKit";

const Toggler = (props) => {
    const [isDisp, setIsDisp] = useState(true);

    const handleToggle = () => {
        setIsDisp(!isDisp);
    }

    return (
        <div>
            <h3>Toggler</h3>
            <Btn onClick={handleToggle}>Toggle</Btn>

            <div>
                {isDisp && props.children}
            </div>
        </div>
    )
}

export default Toggler;