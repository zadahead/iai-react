import { useState } from "react";

import { Btn, Line } from 'UIKit';

const User = (props) => {
    const [isDisp, setIsDisp] = useState(true);

    const handleToggle = () => {
        setIsDisp(!isDisp);
    }
    return (
        <Line>
            <h5>My name is {props.name}</h5>
            {isDisp && <h5>age {props.age}</h5>}
            <Btn onClick={handleToggle}>{isDisp ? 'hide' : 'show'}</Btn>
        </Line>
    )
}

export default User;