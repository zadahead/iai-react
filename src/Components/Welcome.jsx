import User from './User';

const Welcome = (props) => {
    return (
        <div>
            <h1>Welcome</h1>
            <User city="TLV" age={22} name={props.name} />
        </div>
    )
}

export default Welcome;