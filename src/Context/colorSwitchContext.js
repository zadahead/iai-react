import { useColorSwitch } from "Hooks/useColorSwitch";
import { createContext } from "react";

export const colorSwitchContext = createContext({});

const Provider = colorSwitchContext.Provider;

export const ColorSwitchProvider = ({ children }) => {
    const value = useColorSwitch();

    return (
        <Provider value={value}>
            {children}
        </Provider>
    )
}
