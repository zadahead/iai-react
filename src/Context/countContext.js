import { createContext, useState } from "react";

export const countContext = createContext({});

const Provider = countContext.Provider;

export const CountProvider = ({ children }) => {
    const [count, setCount] = useState(0);

    const value = {
        count,
        setCount,
        name: 'Mosh'
    }

    return (
        <Provider value={value}>
            {children}
        </Provider>
    )
}