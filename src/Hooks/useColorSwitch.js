import { useState } from "react";

export const useColorSwitch = () => {

    const [isRed, setIsRed] = useState(true);

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    return {
        handleSwitch,
        color: isRed ? 'red' : 'blue'
    }
}