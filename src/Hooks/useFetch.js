import axios from "axios";
import { useEffect, useState } from "react";

export const useFetch = (endpoint) => {
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState(null);
    const [error, setError] = useState('');

    useEffect(() => {
        axios.get(`https://jsonplaceholder.typicode.com${endpoint}`)
            .then(resp => {
                //const list = resp.data.splice(0, 10);

                setTimeout(() => {
                    setIsLoading(false);
                    setData(resp.data);
                }, 1000)
            })
            .catch(err => {
                setError(err.message);
            })
    }, [])

    return [
        data,
        isLoading,
        error,
        setData
    ]
}