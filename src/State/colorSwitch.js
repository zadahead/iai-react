const { createSlice } = require("@reduxjs/toolkit")

const initialState = {
    color: 'red'
}

const colorSwitchSlice = createSlice({
    name: 'colorSwitch',
    initialState,
    reducers: {
        toggle: (state) => {
            state.color = state.color === 'red' ? 'blue' : 'red'
        }
    }
})

export const colorSwitchActions = colorSwitchSlice.actions;
export const colorSwitchReducer = colorSwitchSlice.reducer;