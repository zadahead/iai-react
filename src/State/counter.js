import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    value: 10,
    name: 'mosh'
}

export const counterSlice = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        add: (state) => {
            state.value += 1;
        },

        reduce: (state) => {
            state.value -= 1;
        },

        amount: (state, action) => {
            state.value += action.payload;
        }
    }
})

export const counterActions = counterSlice.actions;

export const counterReducer = counterSlice.reducer;