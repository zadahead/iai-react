import { configureStore } from "@reduxjs/toolkit";

import { counterReducer } from './counter';
import { Provider } from "react-redux";
import { colorSwitchReducer } from "./colorSwitch";
import { todosReducer } from "./todos";

const store = configureStore({
    reducer: {
        count: counterReducer,
        color: colorSwitchReducer,
        todos: todosReducer
    }
})

export const ReduxProvider = ({ children }) => {
    return (
        <Provider store={store}>
            {children}
        </Provider>
    )
}