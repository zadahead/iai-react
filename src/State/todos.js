const { createSlice } = require("@reduxjs/toolkit")

const initialState = {
    list: []
}

const todosSlice = createSlice({
    name: 'todos',
    initialState,
    reducers: {
        setList: (state, action) => {
            state.list = action.payload.splice(0, 10);
        },

        toggle: (state, action) => {
            console.log(action);
            const index = state.list.findIndex(i => i.id === action.payload.id);
            state.list[index].completed = !state.list[index].completed;
        },

        delete: (state, action) => {
            const index = state.list.findIndex(i => i.id === action.payload.id);
            state.list.splice(index, 1);
        },

        add: (state, action) => {
            state.list.push({
                id: crypto.randomUUID(),
                title: action.payload,
                completed: false
            })
        }
    }
})

export const todosActions = todosSlice.actions;
export const todosReducer = todosSlice.reducer;