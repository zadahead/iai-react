import { Between, Icon } from 'UIKit';

import './Btn.css';
import { forwardRef } from 'react';

export const Btn = forwardRef((props, ref) => {

    return (
        <button
            className='Btn'
            onClick={props.onClick}
            variant={props.variant}
            ref={ref}
        >
            <Between>
                {props.children}
                {props.i && <Icon i={props.i} />}
            </Between>
        </button>
    )
})