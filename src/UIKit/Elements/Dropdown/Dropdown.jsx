import { Between, Icon } from 'UIKit';
import './Dropdown.css';
import { useEffect, useState } from 'react';

export const Dropdown = ({ list = [], selected, onChange }) => {
    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        document.body.addEventListener('click', handleClose);

        return () => {
            document.body.removeEventListener('click', handleClose);
        }
    }, [])

    const handleClose = () => {
        setIsOpen(false);
    }


    const handleToggle = (e) => {
        e.stopPropagation();
        setIsOpen(!isOpen);
    }

    const renderHeader = () => {
        if (selected) {
            const item = list.find(i => i.id === selected);
            if (item) {
                return item.value
            }
        }

        return 'Please Select';
    }

    const getSelectedClass = (i) => {
        return (i.id === selected) ? 'selected' : ''
    }

    const handleSelect = (item) => {
        onChange(item.id);
        setIsOpen(false);
    }

    return (
        <div className='Dropdown'>
            <div className='header' onClick={handleToggle}>
                <Between>
                    <h4>{renderHeader()}</h4>
                    <Icon i="expand_more" />
                </Between>
            </div>
            {isOpen && (
                <div className='list'>
                    {list.map(i => {
                        return (
                            <h5
                                key={i.id}
                                className={getSelectedClass(i)}
                                onClick={() => handleSelect(i)}
                            >
                                {i.value}
                            </h5>
                        )
                    })}
                </div>
            )}
        </div>
    )
}