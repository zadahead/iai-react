import './Icon.css';

export const Icon = (props) => {
    return (
        <span className="Icon material-symbols-rounded" onClick={props.onClick}>
            {props.i || props.children}
        </span>
    )
}
