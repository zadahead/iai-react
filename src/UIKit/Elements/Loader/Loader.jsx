import LoaderSVG from 'assets/loader.svg';
import './Loader.css';

export const Loader = () => {
    return (
        <div className="Loader">
            <img src={LoaderSVG} alt="" />
        </div>
    )
}