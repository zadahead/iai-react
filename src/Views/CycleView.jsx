import User from "Components/User";
import { Rows, Loader, Line, Btn } from "UIKit";
import { useEffect, useState } from "react";


const users = [
    { id: 1, name: 'Mosh', age: 34 },
    { id: 2, name: 'David', age: 44 },
    { id: 3, name: 'Ruth', age: 54 }
];



export const CycleView = () => {
    const [isLoad, setIsLoad] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [list, setList] = useState([]);
    const [count, setCount] = useState(0);

    //console.log('render');

    useEffect(() => {
        //the first time componenet loads/mounted
        //console.log('mounted');
        document.body.addEventListener('click', handleBodyClick);

        return () => {
            //console.log('will unmount');
            document.body.removeEventListener('click', handleBodyClick);
        }
    }, [])

    useEffect(() => {
        console.log('count', count);
        if (count === 3) {
            handleLoad();
        }

        return () => {
            console.log('count #', count);
        }
    }, [count])

    const handleAdd = () => {
        setCount(count + 1);
    }

    const handleBodyClick = () => {
        //console.log('body click');
    }

    const handleLoad = () => {
        setIsLoad(true);
        setIsLoading(true);
        setList([]);

        setTimeout(() => {
            setIsLoading(false);
            setList(users);
        }, 2000);
    }

    const render = () => {
        if (!isLoad) { return }

        return isLoading ? (
            <Loader />
        ) : (
            list.map(u => {
                return <User key={u.id} {...u} />
            })
        )
    }

    return (
        <Rows>
            <Line>
                <h1>Cycle View, {count}</h1>
                <Btn onClick={handleAdd}>Add</Btn>
                <Btn onClick={handleLoad}>Load</Btn>
            </Line>

            {render()}

        </Rows>
    )
}