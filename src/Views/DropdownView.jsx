import { Dropdown, Rows } from "UIKit"
import { useState } from "react"

const list = [
    { id: 1, value: 'item 1' },
    { id: 2, value: 'item 2' },
    { id: 3, value: 'item 3' },
    { id: 4, value: 'item 4' }
]

export const DropdownView = () => {
    const [selected, setSelected] = useState(null);
    return (
        <Rows>
            <h1>Dropdown View</h1>
            <Dropdown
                list={list}
                selected={selected}
                onChange={setSelected}
            />
            <h4>asdasd</h4>
        </Rows>
    )
}