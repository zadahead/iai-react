import { useFetch } from "Hooks/useFetch";
import { Loader, Rows } from "UIKit";



export const FetchView = () => {
    //logic
    const [list, isLoading, error, setTodos] = useFetch('/todos');

    const todos = list ? list.splice(0, 10) : [];

    //view
    const getStyle = (item) => {
        if (item.completed) {
            return {
                color: '#c0c0c0',
                textDecoration: 'line-through'
            }
        }

        return {}
    }

    const handleToggle = (i) => {
        i.completed = !i.completed;
        setTodos([...todos]);
    }

    const renderList = () => {
        if (error) {
            const styleCss = {
                color: 'red'
            }

            return <h3 style={styleCss}>{error}</h3>
        }

        if (isLoading) {
            return <Loader />;
        }

        return (
            todos.map(i => {
                return (
                    <h3
                        key={i.id}
                        style={getStyle(i)}
                        onClick={() => handleToggle(i)}
                    >
                        {i.title}
                    </h3>
                )
            })
        )
    }


    return (
        <Rows>
            <h1>Fetch View</h1>

            {renderList()}
        </Rows>
    )
}