
import { Line, Btn } from 'UIKit';

const HomeView = () => {
    const handleClick = () => {
        console.log('Clicked!!');
    }

    return (
        <div>

            <h1>Welcome to my app</h1>

            <Line>
                <Btn onClick={handleClick}>Click Me</Btn>
                <Btn i="star" onClick={handleClick} variant="cancel" />
                <Btn i="star" onClick={handleClick}>Click Me</Btn>
            </Line>
            <h1>Welcome Home</h1>
        </div>
    )
}

export default HomeView;