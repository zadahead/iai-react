import { ColorSwitchHook } from "Components/ColorSwitchHook"
import { CounterColor } from "Components/CounterColor"
import { CounterHook } from "Components/CounterHook"
import { colorSwitchContext } from "Context/colorSwitchContext"
import { countContext } from "Context/countContext"
import { colorSwitchActions } from "State/colorSwitch"
import { counterActions } from "State/counter"
import { Btn, Line, Rows } from "UIKit"
import { useContext } from "react"
import { useDispatch, useSelector } from "react-redux"

export const HooksView = () => {
    const { count, setCount } = useContext(countContext);
    const { color, handleSwitch } = useContext(colorSwitchContext);

    const store = useSelector(state => state);

    const dispatch = useDispatch();

    const handleAdd = () => {
        setCount(count + 1);
        handleSwitch();
    }

    const handleReduxAdd = () => {
        dispatch(counterActions.amount(5));
        dispatch(colorSwitchActions.toggle());
    }

    const styleCss = {
        backgroundColor: color
    }

    const styleReduxCss = {
        backgroundColor: store.color.color
    }

    return (
        <Rows>
            <Line>
                <h1 style={styleReduxCss}>Store, {store.count.value}</h1>
                <Btn onClick={handleReduxAdd}>Add Redux</Btn>
            </Line>
            <Line>
                <h1 style={styleCss}>Custom Hooks, {count}</h1>
                <Btn onClick={handleAdd}>Add Context</Btn>
            </Line>
            {/* <CounterHook /> */}
            {/* <ColorSwitchHook /> */}
            {/* <CounterColor /> */}
        </Rows>
    )
}