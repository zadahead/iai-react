import { Btn, Rows } from "UIKit"
import { useEffect, useRef, useState } from "react"


export const RefView = () => {
    const [count, setCount] = useState(0);
    const countRef = useRef();
    countRef.current = count;

    const h1Ref = useRef();

    useEffect(() => {
        setTimeout(() => {
            handleAdd();
            changeColor();
        }, 2000)
    }, [])

    const changeColor = () => {
        if (countRef.current > 3) {
            h1Ref.current.style.backgroundColor = 'red';
        }
    }

    const handleAdd = () => {
        setCount(countRef.current + 1);
    }

    return (
        <Rows>
            <h1 >Ref View, {count}</h1>
            <div>
                <Btn onClick={handleAdd} ref={h1Ref}>Add</Btn>
            </div>
        </Rows>
    )
}