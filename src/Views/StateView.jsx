import { useState } from "react";

import Counter from "../Components/Counter";
import Toggler from "../Components/Toggler";

const StateView = () => {
    const [count, setCount] = useState(10);

    return (
        <div>
            <h2>State View</h2>
            <Toggler>
                <Counter add={2} count={count} setCount={setCount} />
            </Toggler>
        </div>
    )
}

export default StateView;