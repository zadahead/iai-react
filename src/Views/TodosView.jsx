import { useFetch } from "Hooks/useFetch"
import { todosActions } from "State/todos"
import { Between, Btn, Icon, Line, Loader, Rows } from "UIKit"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"

export const TodosView = () => {
    const [value, setValue] = useState('');
    const todos = useSelector(state => state.todos);
    const dispatch = useDispatch();

    const [list, isLoading, error] = useFetch('/todos');

    const handleToggle = (item) => {
        dispatch(todosActions.toggle(item))
    }

    const handleDelete = (item) => {
        dispatch(todosActions.delete(item))
    }

    const handleAdd = () => {
        dispatch(todosActions.add(value));
    }

    useEffect(() => {
        if (!isLoading && !error) {
            dispatch(todosActions.setList(list));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isLoading])

    const getIcon = (item) => {
        return item.completed ? 'select_check_box' : 'check_box_outline_blank';
    }

    const getStyle = (item) => {
        if (item.completed) {
            return {
                color: '#d1d1d1',
                textDecoration: 'line-through'
            }
        }

        return {}
    }

    const renderList = () => {
        if (isLoading) {
            return <Loader />
        }

        return todos.list.map(item => {
            return (
                <Between key={item.id}>
                    <Line>
                        {/* select_check_box */}
                        <Icon i={getIcon(item)} onClick={() => handleToggle(item)} />
                        <h4 style={getStyle(item)}>{item.title}</h4>
                    </Line>
                    <Line>
                        <Icon i="delete" onClick={() => handleDelete(item)} />
                    </Line>
                </Between>
            )
        })
    }

    return (
        <Rows>
            <h1>Todos List</h1>

            <Line>
                <input value={value} onChange={(e) => setValue(e.target.value)} />
                <Btn onClick={handleAdd}>Add</Btn>
            </Line>
            {renderList()}
        </Rows>
    )
}