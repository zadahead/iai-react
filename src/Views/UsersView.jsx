import { useState } from "react";

import User from "../Components/User";

import { Btn, Rows } from 'UIKit';

const list = [
    { id: 1, name: 'Mosh', age: 34 },
    { id: 2, name: 'David', age: 44 },
    { id: 3, name: 'Ruth', age: 54 }
];
const UsersView = () => {
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    const renderList = () => {
        const items = list.map((u) => {
            return <User key={u.id} {...u} />
        })

        return items;
    }
    return (
        <Rows>
            <h3>Users View</h3>
            <h4>Users list:, {count}</h4>
            <div>
                <Btn onClick={handleAdd}>Add</Btn>
            </div>
            <Rows>
                {renderList()}
            </Rows>
        </Rows>
    )
}

export default UsersView;