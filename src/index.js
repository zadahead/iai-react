import React from 'react';
import ReactDOM from 'react-dom/client';

import App from './App'
import { BrowserRouter } from 'react-router-dom';
import { CountProvider } from 'Context/countContext';
import { ColorSwitchProvider } from 'Context/colorSwitchContext';
import { ReduxProvider } from 'State/store';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <ReduxProvider>
    <ColorSwitchProvider>
      <CountProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </CountProvider>
    </ColorSwitchProvider>
  </ReduxProvider>
);


